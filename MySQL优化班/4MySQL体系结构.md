# 优化班第十期第五，六次课

## 三层体系结构

![architecure](images/3_tier_architecture.JPG)

连接层 --> SQL层 --> 存储层

### 连接层

* TCP/IP
* UNIX socket
* Name Pipe
* Shared Memory

### SQL层

![sql_parser](images/sql_parser.JPG)

### 存储层

#### MySQL安装文件目录结构

* 配置文件
  * /etc/my.cnf, /etc/mysql/my.cnf ...
* 数据文件
  * frm, MYI, MYD, ibd, ibdata*, ib_logfile* 
* 日志文件
  * error log, general log, binary log, relay log, slow query log

#### 内存结构

![memory_layer](images/memory_layer.png)

* read_rnd_buffer_size
* join_buffer_size
* tmp_table_size/max_heap_table_size
  * 都是session级别
  * max_heap_table_size限制memory表最大容量；内存不够用就不允许写入新的数据，不会转成磁盘表，只会告警超限后拒绝写入
  * tmp_table_size不限制memory表最大容量；如果SQL临时表超过两个size，就会产生基于磁盘的临时表
* internal_tmp_disk_storage_engine
* innodb_buffer_pool_instances

![memory_layer2](images/memory_layer2.png)

#### SQL MODE

* MySQL 5.7默认启用 STRICT_ALL_TABLES 模式
* TRADITIONAL：启用此 SQL 模式可对输入数据值施加类似于其他数据库服务器的限制。在此模式下，使用 GRANT 语句可创建要求指定口令的用户。
* IGNORE_SPACE：默认情况下，必须调用函数名称与后接括号间没有空格的函数。启用此模式后，允许存在此类空格，并使函数名称成为保留字。
* ERROR_FOR_DIVISION_BY_ZERO：默认情况下，除数为零时将产生结果 NULL。在启用此模式的情况下插入数据时，除数为零将导致出现警告，在严格模式下将出现错误。
* ANSI：使用此组合模式将使 MySQL 服务器变得更加“类似于 ANSI”。即，此模式支持的行为更像标准 SQL，如 ANSI_QUOTES 和 PIPES_AS_CONCAT。
* NO_ENGINE_SUBSTITUTION：如果在创建或更改表时指定了不可用的存储引擎，除非启用了此模式，否则 MySQL 将替换默认存储引擎。这是默认的 SQL 模式。

#### InnoDB 内存结构

![innodb_memory](images/innodb_memory.png)

## 关于huge page

* 使用huge page是为了提高内存管理效率
* RHEL6, OEL6, SLES11, UEK2起默认使用
* 透明huge page可以动态调整，无需重启即可生效
* 类似innodb data page的概念
* 查看是否关闭
    * cat /sys/kernel/mm/transparent_hugepage/enabled
    * cat /sys/kernel/mm/transparent_hugepage/defrag

## 常用存储引擎特点对比及选型建议

![engines](images/engines.png)

![engines2](images/engines2.png)


## Plugin 管理

* 查看plugin-dir
    * mysqladmin var | grep plugin_dir
* 安装plugin
    * install plugin rpl_semi_sync_master soname 'semisync_master.so'
* 删除plugin
    * uninstall plugin rpl_semi_sync_master

其他 Plugin

* tokudb
* spider
* handlersocket
* query cache info
* metadata lock info
    * performance_schema.metadata_locks
    * <https://www.percona.com/blog/2016/12/28/quickly-troubleshooting-metadata-locks-mysql-5-7/>
* audit
* memcached
* semi sync replication
* query response time
* password validation and strength checking

## TokuDB

* 大数据存储引擎，开源GPL
* 基于fractal tree index设计
* 高速数据写入场景
* 大数据场景，高效压缩节省空间
* 可以节省一半成本的备选方案
* 支持MVCC，online DDL

TokuDB的特点：

1. 插入性能快20~80倍；
2. 压缩数据减少存储空间；
3. 数据量可以扩展到几个TB；
4. 不会产生索引碎片；
5. 支持hot column addition ， hot indexing， mvcc，多个聚集索引。

适用场景

* 记录数超过五千万以上；随着表的增大，IO增大，innodb写入有拐点
* 数据库大小在1TB以上
* 有数据归档要求
* 字段变化频繁

不适用场景

* 数据量小，1千万一下；innodb的内存结构比tokudb更优秀
* 数据库中存储图片
* 数据库中有大量的update长事务更新

## 虚拟列

* 场景：特殊用途冗余列
* 用法：
    * GENERATED ALWAYS AS (expr)
* 其他：
    * 列长度还是和定义类型一样
    * 也能正常加索引，且查询效率几乎是一样的

## 统计表DML

sys schema

```sql
show tables like 'schema%';
select table_name, rows_fetched, rows_inserted, rows_updated, rows_deleted, io_read_requests
       io_read, io_write_requests, io_write
from sys.schema_table_statistics;
select index_name, rows_selected, rows_inserted, rows_updated, rows_deleted
from sys.schema_index_statistics;
select * from sys.schema_redundant_indexes;
select * from sys.schema_unused_indexes;
select * from sys.schema_tables_with_full_table_scans;
select * from sys.schema_table_statistics_with_buffer;
select * from sys.schema_table_locks_waits;
```

## 一条SQL的生命周期

* 连接层
    * 通信协议处理，检查CS版本，协议等
    * 认证管理，主机、账号和密码
    * 连接管理，线程缓存、线程池、连接池
    * 安全检查
* SQL层
    * 鉴权，判断是否有相应权限
    * 检查QC，SQL解析，改写，优化
    * 提交SQL执行
    * 结果返回