# 优化班第十期第一，二次课

## MySQL和其他DBMS的对比

| 功能          | Oracle | SQL Server | MySQL |
| ----------- | ------ | ---------- | ----- |
| 是否开源/收费     | 闭源，收费  | 闭源，收费      | 开源，免费 |
| 视图/触发器/存储过程 | 支持     | 支持         | 支持    |
| 索引          | 强      | 中          | 中     |
| 事务支持        | 强      | 中          | 强     |
| 复杂查询        | 强      | 中          | 中     |
| 安全性         | 强      | 中          | 强     |
| MVCC        | 支持     | 支持         | 支持    |
| 易学性         | 难      | 简单         | 简单    |
| 用户友好度       | 中      | 强          | 强     |

## 主流分支

* Oracle MySQL
  * 官方正统
* Percona Server
  * 基于InnoDB增加提升性能及易管理性补丁后，形成XtraDB引擎
  * 工具：xtrabackup，percona toolkit
  * 和官方保持一致，基本兼容
* MariaDB
  * 非主流分支，与官方版本区别越来越大，很多不兼容

## Percona Server 特色

* Run faster and more consistent queries
* Consolidate servers on powerful hardware
* Delay sharding, or avoid it entirely
* Save money on hosting fees and power
* Spend less time tuning and administering
* Archive higher uptime

## MariaDB 性能提升

* 优化器性能提升，子查询和JOIN优化
* Windows系统下的InnoDB AIO优化
* checksum table效率更高
* 字符集转化效率更高
* 更早实现了并行复制，线程池，JSON，虚拟列
* 比MySQL 5.7更早实现了客户端链接效率提升

## 关于MySQL DBA

* 运维DBA
  * 日常管理：备份、恢复、故障处理、高可用保障等
  * 整体优化：服务器、网络、MySQL、架构
* 开发DBA
  * 深入理解业务需求
  * SQL优化，编写存储过程、触发器等业务SQL代码
* DB架构师
  * 整体架构设计、优化

六分管理，四分技术；要懂大的框架，不一定要具体到每条SQL都比别人写得好。

数据库要理解。擅长做什么，不擅长做什么。软件开发也要理解。开发语言和开发框架的坑。

## 知识体系

* Hardware
  * 体系架构
  * 阵列卡
  * 硬盘
  * SSD/PCIe SSD：普通SSD也需要加阵列
* OS
  * 初级SA及以上水平
  * 脚本能力
  * 自动化运维
  * 基础安全能力
* Database
  * 索引
  * MVCC
  * 并发控制
  * 执行计划
* Others
  * 云服务
  * 其他数据库
  * 大数据，NoSQL
  * 软实力（沟通表达，情商等）

## 硬件相关

* 了解PC服务器，掌握Dell，HP等常用服务器特点
  * 日常管理，稳定性，可靠性，特殊优化设置，远程管理
* 掌握阵列基础知识（阵列级别、WB/WT、BBU、Cache policy）及如何优化
  * WB，write back
  * WT，write through
  * 正常使用机械硬盘时，WB模式比WT时的IOPS要高一倍以上
* 了解服务器硬件健康状况监控，常见故障处理方案
* 了解硬件关键技术指标
  * CPU：主频，多核，超线程，L1-L3 cache，时延
  * 内存：容量，带宽，时延
  * 硬盘IO：转速，IOPS，吞吐，时延
  * 网络：速率，时延
  * SSD相关：擦写次数，总写入字节数，写放大，平均无故障时长，MLC/SLC
* 如何估算IOPS指标
* 顺序读写，随机读写

## MySQL特点

### 特点一

* 不要当做Oracle，PostgreSQL，或者SQL Server使用
* 不适合的场景
  * 存储大文本，图片，附件等大对象: tfs, ceph, mfs, hdfs
  * 复杂查询，复杂运算，或者全文搜索
* 好的应用场景
  * 小事务快速提交
  * 数据持久化存储
  * 高并发事务控制

### 特点二

* CPU
  * 老版本到新版本，不断优化CPU多核支持
* 内存
  * 简单有效，也有些不足
* IO
  * data file, binlog, redo log, undo log, others
* 其他
  * 单进程，多线程
  * 无连接池
  * 无SQL解析cache

### 特点之CPU

* CPU资源利用特点
  * 5.1之前，多核支持差
  * 5.1，最高可用4个核
  * 5.5，最高可用24个核
  * 5.6，最高可用64个核
  * 5.7，再也不用担心不能好好利用多核特性了
  * 每个query对应一个线程，只能用到一个逻辑core
  * 每个连接对应一个线程，只能用到一个逻辑core
* 建议
  * 物理CPU主频越高越好，核数越多越好
  * 尽可能使用新的版本，抛弃太旧的版本
  * 每次请求尽量快速结束，少用复杂的SQL，事务及时提交或者回滚

Slave exec position不动：可能是因为表没有主键。执行删除时需要做大量的全表扫描

### 特点之内存

* 内存资源利用特点
  * innodb buffer pool, key buffer, query cache类似oracle中的SGA
  * sort buffer, join buffer, tmp table类似oracle中的PGA
  * 官方版本中内存并发竞争锁比较严重，Percona做了优化
* 建议
  * 高并发时，更多内存可以减少物理IO，提高TPS
  * 关闭query cache
  * 通常专用单实例的buffer pool设置为物理内存的50%-70%，多实例时设置总数据量的20%左右
  * KV对象数据存取到redis，memcached；最终需要持久化的数据再存储在MySQL中

### 特点之磁盘

* 磁盘资源利用特点
  * undo log的IO特征：顺序写，随机读
  * redo log, binlog, relay log的IO特征：顺序写，顺序读
  * 数据文件的IO特征：随机写，随机读
  * MyISAM是堆组织表HOT，InnoDB是索引组织表IOT
  * InnoDB相比MyISAM更消耗磁盘空间，但是可以更好地支持并发事务
* 建议
  * 加大内存
  * 使用更高速的IO设备

### MySQL特点之优点

* LAMP/LNMP热门组合
* 开源、免费，可根据实际需求做定制化、个性化开发
* 快速开发、迭代、修复bug，根据需求引入新功能
* 大量的社区资源科提供支持、帮助
* 跨多平台，Linux, Unix, BSD, Windows, Mac
* 特别适合互联网应用
* 权限控制灵活：主机/域名，用户名
* 灵活的多种数据类型支持，没有太多强制限制，可实现内置自动转化，而且可以通过SQL_MODE实现与其他数据库系统兼容
* 灵活的SQL用法，不像其他数据库那么死板
* MySQL复制特性使得基于MySQL的架构设计可以轻松实现快速扩展
* 简单、易用，秒级安装部署完成；可快速上手使用，可快速批量部署、管理，特别适合互联网爆发增长特点
* 对主流开发语言友好，API完善
* 内存分配灵活

### MySQL特点之不足

* 5.6之前无CBO?；5.6有一定的CBO规则?；5.7可以调整cost rule?
  * 貌似一直都是cost based?
* 每个连接、每个query只能使用到一个逻辑CPU
  * 使用小transaction
  * SQL没有并行，只能一个CPU执行
* 随着连接数的增加，性能下降严重；但是有thread pool
  * 线程池到底是怎么运作的？
* online DDL比较弱（使用pt-osc补充）
  * 搞清楚到底哪些DDL可以online
* 官方版本全局锁略多，可使用多实例或者Percona分支版本
* 没有hash join，少用复杂join或者无索引join
* 子查询比较弱，少用或者改成join进行优化
* 优化器比较弱，多用简单的SQL