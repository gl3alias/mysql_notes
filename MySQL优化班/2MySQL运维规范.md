# 优化班第十期第三次课

## MySQL业务场景

* 在线OLTP环境
* 高并发，高TPS
* 业务模型简单
* 用于保障数据可靠性、一致性，以及持久化存储
* 适用业务场景：
  * 在线交易，电商，支付，彩票，在线旅游，物流
  * 传统企业，银行，电信，政府
  * 新闻媒体，各大门户
  * 互动社区，微博，SNS，交友，Linkedin
  * 大数据环境，后端数据存储

## MySQL运行环境部署规范

### 系统安装规范

* 关闭CPU节能，设定为最大性能模式
  * power management
* 关闭NUMA，C-states，C1E
* 阵列卡策略使用Force WB，关闭预读
* 机械盘时，所有盘组成RAID 10阵列
  * <http://www.thecloudcalculator.com/calculators/disk-raid-and-iops.html>
  * <http://imysql.com/2014/09/11/pc-server-raid-controller-disk-health-monitoring.shtml>
* SSD可以只有两块组成RAID 1 或者三块做RAID 5
* xfs/ext4 + deadline/noop
* vm.swappiness <= 10
* vm.dirty_ratio <= 20
* vm.dirty_background_ratio <= 10
* 哪些设置是在用VM的时候也需要注意的？

### MySQL安装规范

* basedir, datadir, backupdir, logdir
* tmpdir 不要太小
* my.cnf options
  * innodb_flush_log_at_trx_commit = 1
  * sync_binlog = 1； 和上面的一起，双一，保证数据安全性（不丢失）
  * innodb_file_per_table = 1
  * innodb_data_file_path设置ibdata1至少1GB以上
  * long_query_time <= 0.5
  * lower_case_table_names = 0

### 开发环境

* 启用long_queries_not_using_indexes
* 设置long_query_time为最小值
* 定期检查分析slow log
* 授权和生产环境一致
* 关闭query cache
* 设置较小的innodb_buffer_pool_size和key_buffer_size：更早发现性能问题
* 数据量不能太少，否则有些性能问题无法提前规避

### 和业务配合

* 批量导入、导出数据前需要提前通知DBA，请求协助观察
* 推广活动或上线新功能前需提前通知DBA，请求压力评估
* 不能使用SUPER权限连接数据库
* 单表多次ALTER操作必须合并为一次操作
* 数据库DDL及重要的SQL需要及早提交DBA评审
* 重要业务库需告知DBA重要等级、数据备份及时性要求
* 不在业务高峰期批量更新、查询数据库
* 提交线上DDL需求，所有的SQL语句需要有备注说明
