# SQL Performance Explained

The main benefit of SQL is the capability to separate "what" and "how".

The author (of SQL statements) must know about the database to prevent performance problems.

Database indexing is a development task. Because the most important information for proper indexing is not the storage system configuration or the hardware setup, rather it's how the appilcation queries the data. This knowledge (the access path) is not very accessible to the DBAs or external consultants. The developers have this information anyways.

## 1. Anatomy of an Index

An index makes the query fast.

The index requires its own disk space and holds a copy of the indexed table data. That means that an index is pure redundancy.

Finding data in an ordered data set is fast and easy because the sort order determines each entry’s position.

The database combines two data structures to meet the challenge: **a doubly linked list and a search tree**.

### **The Inex Leaf Nodes**

The primary purpose of an index is to provide an ordered representation of the indexed data.

Doubly Linked List:

* Every node has links to two neighboring entries, very much like a chain. New nodes are inserted between two existing nodes by updating their links to refer to the new node. The physical location of the new node doesn’t matter because the doubly linked list maintains the logical order.
* The data structure is called a doubly linked list because each node refers to the preceding and the following node.

Databases use doubly linked lists to connect the so-called index leaf nodes. Each leaf node is stored in a database block or page; that is, the database’s smallest storage unit. All index blocks are of the same size — typically a few kilobytes. The database uses the space in each block to the extent possible and stores as many index entries as possible in each block. That means that the **index order is maintained on two different levels: the index entries within each leaf node, and the leaf nodes among each other using a doubly linked list.**

![figure1.1.JPG](figure1.1.JPG)

### **The Search Tree (BTree)**

The index leaf nodes are stored in an arbitrary order — the position on the disk does not correspond to the logical position according to the index order. A database needs a second structure to find the entry among the shuffled pages quickly: a balanced search tree — in short: the B-tree.

![figure1.2.JPG](figure1.2.JPG)

The structure is a balanced search tree because the tree depth is equal at every position; the distance between root node and leaf nodes is the same everywhere. A B-tree is a balanced tree — not a binary tree.

![figure1.3.JPG](figure1.3.JPG)

**First power of indexing: tree traversal.**

* works almost instantly, even on a huge data set
* tree balance allows accessing all elements with the same number of steps
* logarithmic growth of the tree depth

### **Slow Indexes Part I**

Myth: index rebuild helps with slow SQL statements

Reasons:

1. First ingredient of slow index lookup: leaf node chain. Index lookup may not only needs to perform the tree traversal, it may also need to follow the leaf node chain.
1. Second ingredient: accessing the table.

An index lookup requires three steps:

1. the tree traversal;
1. following the leaf node chain;
1. fetching the table data.

The tree traversal is the only step that has an upper bound for the number of accessed blocks — the index depth. The other two steps might need to access many blocks — they cause a slow index lookup.