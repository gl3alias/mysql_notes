# Chapter 3: Selecting Data from Tables

## 3.1. Specifying Which Columns and Rows to Select

```sql
select * from mail;
select srcuser, srchost, t, size from mail;
select t, srcuser, srchost from mail where srchost = 'venus';
select t, srcuser, srchost from mail where srchost like 's%';
select * from mail where srcuser = 'barb' and dstuser = 'tricia';
select t, concat(srcuser, '@', srchost), size from mail;
```

## 3.2. Naming Query Result Columns

```sql
select date_format(t, '%M %e, %Y'), concat(srcuser, '@', srchost), size from mail;
select date_format(t, '%M %e, %Y') as date_sent,
       concat(srcuser, '@', srchost) as sender, size
from mail;

select t, srcuser, dstuser, size/1024 as kilobytes
from mail where size/1024 > 500; -- Column alias doesn't work in WHERE clause.
```

## 3.3. Sorting Query Results

```sql
select * from mail where dstuser = 'tricia' order by srchost, srcuser;
select * from mail where size > 50000 order by size desc;
```

## 3.4. Removing Duplicate Rows

```sql
select srcuser from mail;
select distinct srcuser from mail;
select count(distinct srcuser) from mail;
select distinct year(t), month(t), dayofmonth(t) from mail;
```

## 3.5. Working with NULL Values

```sql
select * from expt where score is not null;
select null = null, null <=> null;
select subject, test, if(score is null, 'unknown', score) as 'score' from expt;
```

## 3.6. Writing Comparisons Involving NULL in Programs

```perl
$operator = defined ($score) ? "=" : "IS";
# $operator = defined ($score) ? "<>" : "IS NOT";
$sth = $dbh->prepare("select * from expt where score $oeprator ?");
$sth->execute ($score)

# how to do this in Python?
```

## 3.7. Using Views to Simplify Table Access

```sql
select date_format(t, '%M %e, %Y') as date_sent,
       concat(srcuser, '@', srchost) as sender,
       concat(dstuser, '@', dsthost) as recipient,
       size
from mail;

create view mail_view as
select date_format(t, '%M %e, %Y') as date_sent,
       concat(srcuser, '@', srchost) as sender,
       concat(dstuser, '@', dsthost) as recipient,
       size
from mail;

select date_sent, sender, size from mail_view where size > 100000 order by size;
```

## 3.8. Selecting Data from Multiple Tables

```sql
select id, name, service, contact_name from profile inner join profile_contact on id = profile_id;
select * from profile_contact where profile_id = (select id from profile where name = 'Nancy');
```

3.9. Selecting Rows from the Beginning, End, or Middle of Query Results

```sql
select * from profile limit 3;
select * from profile order by birth limit 1;
select name, date_format(birth, '%m-%d') as birthday
from profile order by birthday limit 1;
select * from profile order by birth desc limit 2, 1;

select sql_calc_found_rows * from profile order by name limit 4;
select found_rows();
```

## 3.10. What to Do When LIMIT Requires the “Wrong” Sort Order

```sql
select name, birth from profile order by birth desc limit 4; -- showing last 4 but order is desc
select * from
(select name, birth from profile order by birth desc limit 4) as t
order by birth; -- also showing last 4 but order is asc
```

## 3.11. Calculating LIMIT Values from Expressions

Arguments to LIMIT must be literal integers, not expressions.

```sql
-- illegal statements
select * from profile limit 5+5;
select * from profile limit @skip_count, @show_count;
```