# Chapter 6: Working with Dates and Times

## 6.1. Choosing a Temporal Data Type

To choose a temporal data type, consider questions such as these:

* Do you need times only, dates only, or combined date and time values?
* What range of values do you require?
* Do you want automatic initialization of the column to the current date and time?

### Temporal Data Types

* DATE values have CCYY-MM-DD format, where CC, YY, MM, and DD represent the century, year within century, month, and day parts of the date. 1000-01-01 to 9999-12-31
* TIME values have hh:mm:ss format, where hh, mm, and ss are the hours, minutes, and seconds parts of the time. -838:59:59 to 838:59:59
* DATETIME and TIMESTAMP are combined date-and-time values in CCYY-MM-DD hh:mm:ss format
  * DATETIME has a supported range of 1000-01-01 00:00:00 to 9999-12-31 23:59:59, whereas TIMESTAMP values are valid only from the year 1970 partially through 2038
  * TIMESTAMP and DATETIME have special auto-initialization and auto-update properties, but for DATETIME they are not available before MySQL 5.6.5.
  * When a client inserts a TIMESTAMP value, the server converts it from the timezone associated with the client session to UTC and stores the UTC value. When the client retrieves a TIMESTAMP value, the server performs the reverse operation to convert the UTC value back to the client session time zone.
* Types that include a time part can have a fractional seconds part for subsecond resolution

## 6.2. Using Fractional Seconds Support

As of MySQL 5.6.4, fractional seconds are supported for temporal types that include a time part: DATETIME, TIME, and TIMESTAMP

```sql
-- to create a TIME column with two fractional digits
mycol TIME(2)

-- col(fsp)
select curtime(), curtime(2), curtime(6);
```

## 6.3. Changing MySQL’s Date Format

MySQL always stores dates in ISO format, a fact with implications both for data entry (input) and for displaying query results (output):

* For data-entry purposes, to store values that are not in ISO format, you normally must rewrite them first.
* For display purposes, you can rewrite dates to non-ISO formats.
  * DATE_FORMAT()
  * TIME_FORMAT()
  * YEAR()
  * STR_TO_DATE()

```sql
insert into date_val (d) values(STR_TO_DATE('May 13, 1987', '%M %d, %Y'));
select d from date_val;

select d, DATE_FORMAT(d, '%M %d, %Y') from date_val;
```

| Sequence | Meaning                           |
| -------- | --------------------------------- |
| %Y       | Four-digit year                   |
| %y       | Two-digit year                    |
| %M       | Complete month name               |
| %b       | Month name, initial three letters |
| %m       | Two-digit month of year (01..12)  |
| %c       | Month of year (1..12)             |
| %d       | Two-digit day of month (01..31)   |
| %e       | Day of month (1..31)              |
| %W       | Weekday name (Sunday..Saturday)   |
| %r       | 12-hour time with AM or PM suffix |
| %T       | 24-hour time                      |
| %H       | Two-digit hour                    |
| %i       | Two-digit minute                  |
| %s       | Two-digit second                  |
| %%       | Literal %                         |

```sql
select dt, DATE_FORMAT(dt, '%c/%e/%y %r') AS format1, DATE_FORMAT(dt, '%M %e, %Y %T') AS format2 from datetime_val;

select dt, TIME_FORMAT(dt, '%r') AS '12-hour time', TIME_FORMAT(dt, '%T') AS '24-hour time' from datetime_val;

create function time_ampm (t TIME)
returns varchar(13) # mm:dd:ss {a.m.|p.m.} format
deterministic
return concat(left(time_format(t, '%r'), 9), if(time_to_sec(t) < 12*60*60, 'a.m.', 'p.m.'));

select t1, time_ampm(t1) from time_val;
```

## 6.4. Setting the Client Time Zone

Time zone settings have an important effect on TIMESTAMP values:

* When the MySQL server starts, it examines its operating environment to determine its time zone.
  * use --default-time-zone to change
* For each client that connects, the server interprets TIMESTAMP values with respect to the time zone associated with the client session.
  * When a client inserts a TIMESTAMP value, the server converts it from the client time zone to UTC and stores the UTC value.
  * When the client retrieves a TIMESTAMP value, the server performs the reverse operation to convert the UTC value back to the client time zone.
* The default session time zone for each client when it connects is the server time zone. If all clients are in the same time zone as the server, nothing special need be done for proper TIMESTAMP time zone conversion to occur. But if a client is in a time zone different from the server and it inserts TIMESTAMP values without making the proper time zone correction, the UTC values won’t be correct.

```sql
create table t (ts timestamp);
insert into t (ts) values ('2014-06-01 12:30:00');
select ts from t;

-- if client is in same timezone as server
select @@global.time_zone, @@session.time_zone;
select ts from t;

-- if client is in different timezone as server
set session time_zone = '-04:00';
select @@global.time_zone, @@session.time_zone;
select ts from t;
```

## 6.5. Shifting Temporal Values Between Time Zones

```sql
set @dt = '2017-07-26 09:00:00';
select @dt as Chicago, CONVERT_TZ(@dt, 'US/Central', 'Europe/Berlin') as Berlin,
       CONVERT_TZ(@dt, 'US/Central', 'Europe/London') as London,
       CONVERT_TZ(@dt, 'US/Central', 'America/Edmonton') as Edmonton,
       CONVERT_TZ(@dt, 'US/Central', 'Australia/Brisbane') as Brisbane\G

select @dt as Chicago, CONVERT_TZ(@dt, '-06:00', '+01:00') as Berlin,
       CONVERT_TZ(@dt, '-06:00', '+00:00') as London,
       CONVERT_TZ(@dt, '-06:00', '-07:00') as Edmonton,
       CONVERT_TZ(@dt, '-06:00', '+10:00') as Brisbane\G
```

## 6.6. Determining the Current Date or Time

```sql
select curdate(), curtime(), now();

select utc_date(), utc_time(), utc_timestamp();
```

## 6.7. Using TIMESTAMP or DATETIME to Track Row-Modification Times

* A TIMESTAMP or DATETIME column declared with the DEFAULT CURRENT_TIME STAMP attribute initializes automatically for new rows. Simply omit the column from INSERT statements and MySQL sets it to the row-creation time.
* A TIMESTAMP or DATETIME column declared with the ON UPDATE CURRENT_TIME STAMP attribute automatically updates to the current date and time when you change any other column in the row from its current value.

```sql
CREATE TABLE tsdemo
(
  val INT,
  ts_both   TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ts_create TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  ts_update TIMESTAMP DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP
);

-- Demonstrate that inserting no explicit value inserts the current
-- timestamp into the TIMESTAMP and DATETIME columns. Also demonstrate
-- that inserting NULL inserts current timestamp into TIMESTAMP columns.

INSERT INTO tsdemo (val) VALUES(5);
SET @dummy = SLEEP(5);
INSERT INTO tsdemo (val,ts_both,ts_create,ts_update)
VALUES(10,NULL,NULL,NULL);

SELECT * FROM tsdemo;

-- Change one row; TIMESTAMP columns that auto-update in that row will update

SET @dummy = SLEEP(4);
UPDATE tsdemo SET val = 11 WHERE val = 10;
SELECT * FROM tsdemo;

-- Change all rows; TIMESTAMP columns that auto-update in all rows will update

SET @dummy = SLEEP(6);
UPDATE tsdemo SET val = val + 1;
SELECT * FROM tsdemo;

-- An update that changes no rows (doesn't change any TIMESTAMP columns)

SET @dummy = SLEEP(2);
UPDATE tsdemo SET val = val;
SELECT * FROM tsdemo;
```

## 6.8. Extracting Parts of Dates or Times

| Function     | Return value                            |
| ------------ | --------------------------------------- |
| YEAR()       | Year of date                            |
| MONTH()      | Month number (1..12)                    |
| MONTHNAME()  | Month name (January..December)          |
| DAYOFMONTH() | Day of month (1..31)                    |
| DAYNAME()    | Day name (Sunday..Saturday)             |
| DAYOFWEEK()  | Day of week (1..7 for Sunday..Saturday) |
| WEEKDAY()    | Day of week (0..6 for Monday..Sunday)   |
| DAYOFYEAR()  | Day of year (1..366)                    |
| HOUR()       | Hour of time (0..23)                    |
| MINUTE()     | Minute of time (0..59)                  |
| SECOND()     | Second of time (0..59)                  |
| EXTRACT()    | Varies                                  |

```sql
select dt, year(dt), dayofmonth(dt), hour(dt), second(dt) from datetime_val;
select d, dayofyear(d) from date_val;
select d, dayname(d), left(dayname(d), 3) from date_val;
select d, dayname(d), dayofweek(d), weekday(d) from date_val;
select dt, extract(day from dt), extract(hour from dt) from datetime_val;

-- to obtain current year, month, day, or day of week
select curdate(), year(curdate()) as year, month(curdate()) as month, monthname(curdate()) as monthname,
       dayofmonth(curdate()) as day, dayname(curdate()) as dayname;
-- to obtain current hour, minute, or second
select now(), hour(now()) as hour, minute(now()) as minute, second(now()) as second;

-- reformat date values
select dt, date_format(dt, '%Y') as year, date_format(dt, '%d') as day,
       time_format(dt, '%H') as hour, time_format(dt, '%s') as second
from datetime_val;

-- formatting functions are easier to use when extracting more than one part of a value
-- or display extracted values in a format different from the default
select dt, date_format(dt, '%Y-%m-%d') as 'date part', time_format(dt, '%T') as 'time part' from datetime_val;
select dt, date_format(dt, '%M %e, %Y') as 'descriptive date', time_format(dt, '%H:%i') as 'hours/minutes' from datetime_val;
```

## 6.9. Synthesizing Dates or Times from Component Values

* Use MAKETIME() to construct a TIME value from hour, minute, and second parts.
* Use DATE_FORMAT() or TIME_FORMAT() to combine parts of the existing value with parts you want to replace.
* Pull out the parts that you need with component-extraction functions and recombine the parts with CONCAT().

```sql
select maketime(10, 30, 58), maketime(-5, 0, 11);
select d, date_format(d, '%Y-%m-01') from date_val;
select t1, time_format(t1, '%H:%i:00') from time_val;
select d, concat(year(d), '-', month(d), '-01') from date_val;
select d, concat(year(d), '-', lpad(month(d), 2, '0'), '-01') from date_val; -- with padding
select t1, concat(lpad(hour(t1), 2, '0'), ':', lpad(minute(t1), 2, '0'), ':00') as recombined from time_val;

set @d = '2014-02-28', @t = '13:10:05';
select @d, @t, concat(@d, ' ', @t);
```