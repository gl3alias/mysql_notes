# Chapter 5: Working with Strings

## 5.1. String Properties

```sql
show character set;
show collation like 'latin1';

set @s = convert('abc' using utf8);
select length(@s), char_length(@s);

set @s = convert('abc' using ucs2);
select length(@s), char_length(@s);

select c from t order by c collate latin1_swedish_ci;
select c from t order by c collate latin1_general_cs;
select c from t order by c collate latin1_bin;

select c from t order by c collate utf8_general_ci;
select c from t order by c collate utf8_spanish2_ci;
```

## 5.2. Choosing a String Data Type

* Are the strings binary or nonbinary?
* Does case sensitivity matter?
* What is the maximum string length?
* Do you want to store fixed- or variable-length values?
* Do you need to retain trailing spaces?
* Is there a fixed set of permitted values?

| Binary data type | Nonbinary data type | Maximum length |
| ---------------- | ------------------- | -------------- |
| BINARY           | CHAR                | 255            |
| VARBINARY        | VARCHAR             | 65,535         |
| TINYBLOB         | TINYTEXT            | 255            |
| BLOB             | TEXT                | 65,535         |
| MEDIUMBLOB       | MEDIUMTEXT          | 16,777,215     |
| LONGBLOB         | LONGTEXT            | 4,294,967,295  |

A table can include a mix of binary and nonbinary string columns, and its nonbinary columns can use different character sets and collations.

```sql
create table mytable (utf8str varchar(100) character set utf8 collate utf8_danish_ci,
                      sjisstr varchar(100) character set sjis collate sjis_japanese_ci);
```

## 5.3. Setting the Client Connection Character Set

Use SET NAMES or an equivalent method to set your connection to the proper character set.

```sql
-- option 1:
set names 'utf8';
set names 'utf8' collate 'utf8_general_ci';

-- option 2: config file
[mysql]
default-character-set = utf8

-- option 3: LANG or LC_ALL environment variables on Unix
set LC_ALL to en_US.UTF-8

-- option 4: connector, API (better way than SET NAMES)
jdbc:mysql://localhost/cookbook?characterEncoding=UTF-8
db = MySQLdb.connect(host="localhost", user='root', passwd='passwd', db='sandbox', use_unicode=True, charset="utf8")
conn_params = {
"database": "cookbook",
"host": "localhost",
"user": "cbuser",
"password": "cbpass",
"charset": "utf8",
}
```

## 5.4. Writing String Literals

```sql
-- option 1: quotes
'my string'
"my string" -- cannot use double quotes if ANSI_QUOTES is enabled

-- option 2: hexadecimal notation
-- abcd can be writen as
0x61626364
X'61626364'
x'61626364'
select X'61626364' from dual;

-- option 3: use introducer, tells the server how to interpret the string that follows it.
_latin1 'abcd'
_ucs2 'abcd'
```

## 5.5. Checking or Changing a String’s Character Set or Collation

```sql
create table t (c char(10) character set utf8 collate utf8_danish_ci);
select user(), charset(user()), collation(user());

set names 'latin1';
select charset('abc'), collation('abc');
set names utf8 collate 'utf8_bin';
select charset('abc'), collation('abc');

set @s1 = _latin1 'my string', @s2 = convert(@s1 using utf8);
select charset(@s1), charset(@s2);

set @s1 = _latin1 'my string';
set @s2 = convert(@s1 using utf8) collate utf8_spanish_ci;
select charset(@s1), collation(@s1), charset(@s2), collation(@s2);

set @s1 = _latin1 'my string';
set @s2 = convert(@s1 using binary);
set @s3 = convert(@s2 using utf8);
select charset(@s1), charset(@s2), charset(@s3);

select charset(binary _latin1 'my string');
```

## 5.6. Converting the Lettercase of a String

```sql
select first_name, upper(first_name), lower(first_name) from employees.employees limit 5;

-- upper and lower functions do not work with binary strings
create table t (b BLOB) select 'aBcD' as b;
select b, upper(b), lower(b) from t;
-- convert to nonbinary string first
select b, upper(convert(b using latin1)) as upper, lower(convert(b using latin1)) as lower from t;

-- convert part of a string
create function initial_cap (s varchar(255))
returns varchar(255) deterministic
return concat(upper(left(s, 1)), mid(s,2));
select lower(first_name), initial_cap(lower(first_name)) from employees.employees limit 5;
```

## 5.7. Controlling Case Sensitivity in String Comparisons

```sql
select 'cat' = 'cat', 'cat' = 'dog', 'cat' <> 'cat', 'cat' <> 'dog';
select 'cat' < 'awk', 'cat' < 'dog', 'cat' between 'awk' and 'eel';
```

A binary string is a sequence of bytes and is compared using numeric byte values. Lettercase has no meaning. However, because letters in different cases have different byte values, comparisons of binary strings effectively are case sensitive.

```sql
set @s1 = binary 'cat', @s2 = binary 'CAT';
select @s1 = @s2;
set @s1 = convert(@s1 using latin1) collate latin1_general_ci;
set @s2 = convert(@s2 using latin1) collate latin1_general_ci;
select @s1 = @s2;

-- compare 2 nonbinary strings as binary strings
select @s1 = _latin1 'cat', @s2 = _latin1 'CAT';
select @s1 = @s2, binary @s1 = b
```

## 5.8. Pattern Matching with SQL Patterns

```sql
select name from metal where name like 'me%';
select name from metal where name like '%d';
select name from metal where name like '%in%';
select name from metal where name like '__at%';

select name from metal where name not like '%i%';

-- SQL patterns do not match NULL values
select NULL like '%', NULL not like '%';
```

## 5.9. Pattern Matching with Regular Expressions

| Pattern | What the pattern matches                             |
| ------- | ---------------------------------------------------- |
| ^       | Beginning of string                                  |
| $       | End of string                                        |
| .       | Any single character                                 |
| [...]   | Any character listed between the square brackets     |
| [^...]  | Any character not listed between the square brackets |
| *       | Zero or more instances of preceding element          |
| +       | One or more instances of preceding element           |
| {n}     | n instances of preceding element                     |
| {m,n}   | m through n instances of preceding element           |

```sql
select name from metal where name REGEXP '^me';
select name from metal where name REGEXP 'd$';
select name from metal where name REGEXP 'in';
select name from metal where name REGEXP '^..at';
```

| POSIX class | What the class matches               |
| ----------- | ------------------------------------ |
| [:alnum:]   | Alphabetic and numeric characters    |
| [:alpha:]   | Alphabetic characters                |
| [:blank:]   | Whitespace (space or tab characters) |
| [:cntrl:]   | Control characters                   |
| [:digit:]   | Digits                               |
| [:graph:]   | Graphic (nonblank) characters        |
| [:lower:]   | Lowercase alphabetic characters      |
| [:print:]   | Graphic or space characters          |
| [:punct:]   | Punctuation characters               |
| [:space:]   | Space, tab, newline, carriage return |
| [:upper:]   | Uppercase alphabetic characters      |
| [:xdigit:]  | Hexadecimal digits (0-9, a-f, A-F)   |

```sql
-- character classes

-- values that contain any hexadecimal digit character:
select name, name REGEXP '[[:xdigit:]]' from metal;

-- strings that begin with a vowel or end with d:
select name from cookbook.metal where name REGEXP '^[aeiou]|d$';

-- groups
-- match strings that consist entirely of digits or entirely of letters
select '0m' REGEXP '^[[:digit:]]+|[[:alpha:]]+$'; -- won't work
select '0m' REGEXP '^([[:digit:]]+|[[:alpha:]]+)$';

-- regex do not match NULL values
select NULL REGEXP '.*', NULL NOT REGEXP '.*';
```

## 5.10. Breaking Apart or Combining Strings

```sql
-- left(), right(), mid()
set @date = '2015-07-21';
select @date, left(@date, 4) as year, mid(@date, 6, 2) as month, right(@date, 2) as day;

-- substring()
select @date, substring(@date, 6), mid(@date, 6);

-- substring_index(); case sensitive
set @email = 'postmaster@example.com';
select @email, substring_index(@email, '@', 1) as user, substring_index(@email, '@', -1) as host;

-- comparisons
select name from metal where left(name, 1) >= 'n';

-- concat
select concat(name, ' ends in "d": ', if(right(name, 1)='d', 'YES', 'NO')) as 'ends in "d"?' from cookbook.metal;
update metal set name = concat(name, 'ide'); -- in place update
update metal set name = left(name, char_length(name) - 3);
```

## 5.11. Searching for Substrings

```sql
-- find the position at which the substring occurs
select name, locate('in', name), locate('in', name, 3) from metal;

-- check whether the substring is present and don't care about its position
select name, name like '%in%', name REGEXP 'in' from metal;
```

## 5.12. Using Full-Text Searches

By default, full-text searches compute a relevance ranking and use it for sorting.

```sql
select count(*) from kjv where match(vtext) against('Hadoram');
select bname, cnum, vnum, left(vtext, 65) as vtext from kjv where match(vtext) against('Hadoram')\G

select count(*) from kjv where match(vtext) against('Abraham');
select count(*) from kjv where match(vtext) against('Abraham') and bname = 'Hebrews';

-- multi column fulltext index
alter table tbl_name add fulltext (col1, col2, col3);
select ... from tbl_name where match(col1, col2, col3) against('search string');
```

## 5.13. Using a Full-Text Search with Short Words

```sql
select count(*) from kjv where match(vtext) against('god'); -- no rows
select count(*) from kjv where match(vtext) against('sin'); -- no rows

-- ft_min_word_len = 3 then reboot; only applies to myisam
-- innodb_ft_min_token_size for innodb
repair table kjv quick; -- for myisam;
alter table kjv drop index vtext, add fulltext(vtext);
select count(*) from kjv where match(vtext) against('god');
select count(*) from kjv where match(vtext) against('sin');
```

## 5.14. Requiring or Prohibiting Full-Text Search Words

```sql
select count(*) from kjv where match(vtext) against('David Goliath'); -- match rows with either David or Goliath
select count(*) from kjv where match(vtext) against('David') and match(vtext) against('Goliath');
select count(*) from kjv where match(vtext) against('+David +Goliath' in boolean mode);
select count(*) from kjv where match(vtext) against('+David -Goliath' in boolean mode);

select count(*) from kjv where match(vtext) against('whirl*' in boolean mode);
```

## 5.15. Performing Full-Text Phrase Searches

```sql
-- to find rows that contain a particular phrase
select count(*) from kjv where match(vtext) against('still small voice'); -- this won't work

select count(*) from kjv where match(vtext) against('"still small voice"' in boolean mode);
```