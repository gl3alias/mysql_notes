# Chapter 4: Table Management

## 4.1. Cloning a Table

```sql
create table new_table like original_table;
insert into new_table select * from original_table;

create table mail2 like mail;
insert into mail2 select * from mail where srcuser = 'barb';
```

## 4.2. Saving a Query Result in a Table

```sql
insert into dst_tbl (i, s) select val, name from src_tbl;
insert into dst_Tbl select * from src_tbl where val > 100 and name like 'A%';
insert into dst_tbl (i, s) select count(*), name from src_tbl group by name;

create table dst_tble (id int not null auto_increment, primary key (id)) select a, b, c from src_tbl;

create table dst_tbl select inv_no, sum(unit_cost*quantity) as total_cost from src_tbl group by inv_no;

create table dst_tbl (primary key (id), index(state, city)) select * from src_tbl;

-- auto_increment not copied
create table dst_tbl (primary key (id)) select * from src_tbl;
alter table dst_tbl modify id int unsigned not null auto_increment;
```

## 4.3. Creating Temporary Tables

```sql
create temporary table mail select * from mail;
select count(*) from mail;
delete from mail;
select count(*) from mail;
drop temporary table mail;
select count(*) from mail;
```

## 4.4. Generating Unique Table Names

```python
import os

tbl_name = "tmp_tbl_%d" % os.getpid()
```

```sql
set @tbl_name =concat('tmp_tbl_', connection_id());
set @stmt = concat('drop table if exists ', @tbl_name)
prepare stmt from @stmt;
execute stmt;
deallocate prepare stmt;
set @stmt = concat('create table ', @tbl_name, ' (i int)')
prepare stmt from @stmt;
execute stmt;
deallocate prepare stmt;
```

## 4.5. Checking or Changing a Table Storage Engine

```sql
select engine from information_schema.tables where table_schema='cookbook' and table_name = 'mail';
show table status like 'mail'\G
show create table mail\G

alter table mail engine = innodb;
```

## 4.6. Copying a Table Using mysqldump

```bash
mysqldump cookbook mail > mail.sql
mysql cookbook < mail.sql

mysqldump cookbook mail | mysql -h another-host otherdb
mysqldump cookbook mail | ssh another-host mysql otherdb
```